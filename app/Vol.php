<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vol extends Model
{
    protected $table = "vols";
    protected $primaryKey = "id";
    protected $guarded = [];

    public function reservations()
    {
        return $this->hasMany('App\Reservation', "vol_id");
    }
}
