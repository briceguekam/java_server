<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table = "reservations";
    protected $primaryKey = "id";
    protected $guarded = [];

    public function client()
    {
        return $this->hasOne('App\Client');
    }
    public function vol()
    {
        return $this->hasOne('App\Vol');
    }

}
