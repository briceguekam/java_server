<?php

namespace App\Http\Controllers;

use App\vol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render(Vol::all(), true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new \App\Vol([
            "plane_id" => $request->avion_id,
            "capacity" => $request->capacity,
            "city_start" => $request->ville_depart,
            "city_end" => $request->ville_arrive,
            "hour_start" => $request->heure_depart,
            "hour_end" => $request->heure_arriver,
        ]);
        $client->save();
        return $this->render("Vol sauvegarder", true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vol $vol
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $passager = DB::select("select clients.* from clients, reservations, vols where clients.id = reservations.client_id and vols.id = reservations.vol_id and reservations.vol_id = ?", [
            $request->id
        ]);
        return $this->render($passager, true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vol $vol
     * @return \Illuminate\Http\Response
     */
    public function edit(vol $vol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\vol $vol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $vol = Vol::find($request->id);
        $vol->plane_id = $request->avion_id;
        $vol->capacity = $request->capacity;
        $vol->city_start = $request->ville_depart;
        $vol->city_end = $request->ville_arrive;
        $vol->hour_start = $request->heure_depart;
        $vol->hour_end = $request->heure_arriver;
        $vol->update();
        return $this->render("Vol mis a jour", true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vol $vol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Vol::destroy($request->id);
        return $this->render("Vol supprimer avec success", true);

    }
}
