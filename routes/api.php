<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/client/update", "ClientController@update");
Route::post("/client", "ClientController@store");
Route::get("/client/delete", "ClientController@destroy");
Route::get("/client/find", "ClientController@show");
Route::get("/client", "ClientController@index");





Route::post("/vol/update", "VolController@update");
Route::post("/vol", "VolController@store");
Route::get("/vol/delete", "VolController@destroy");
Route::get("/vol/find", "VolController@show");
Route::get("/vol", "VolController@index");


Route::get("/reservation/add", "ReservationController@store");
Route::get("/reservation/delete", "ReservationController@destroy");
Route::get("/reservation/find", "ReservationController@show");

