<?php

use Illuminate\Database\Seeder;

class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 15; $i++){
            $reservation = new \App\Reservation([
                "client_id" =>  ($i + 1),
                "vol_id" =>  ($i / 5) + 1,
            ]);
            $reservation->save();
        }
    }
}
