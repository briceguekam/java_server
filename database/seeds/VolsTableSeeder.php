<?php

use Illuminate\Database\Seeder;

class VolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 3; $i++){
            $vol = new \App\Vol([
                "plane_id" =>  $i,
                "capacity" => 10 * ($i + 1),
                "city_start" => "Ville 1",
                "city_end" => "Ville 2",
                "hour_start" => "10H-42",
                "hour_end" => "15H-42",
            ]);
            $vol->save();
        }
    }
}
