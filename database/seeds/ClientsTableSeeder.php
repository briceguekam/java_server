<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 15; $i++){
            $client = new \App\Client([
                "name" => "client " . $i,
                "address" => "address du client  " . $i,
            ]);
            $client->save();
        }
    }
}
